function distance(first, second){

if( !Array.isArray(first) || !Array.isArray(second) )
{
  throw new Error('InvalidType');
}

var firstLength = first.length;
var secondLength = second.length;

if(firstLength == 0 && secondLength == 0)
{
  return 0;
}

var setFirst = new Set(); 
var setSecound = new Set(); 

for (var i = 0; i < firstLength; i++) 
{
  setFirst.add(first[i]);
}
for(var j = 0; j < secondLength; j++)
{
  setSecound.add(second[j]);
}

firstSetArray = Array.from(setFirst);
secondSetArray = Array.from(setSecound);

var firstSetLength = firstSetArray.length;
var secondSetLength = secondSetArray.length;

var sumElement = firstSetLength + secondSetLength;
var mutualElement = 0;

for (var i = 0; i < firstSetLength; i++) 
{
  for(var j = 0; j < secondSetLength; j++)
  {
    if(firstSetArray[i] === secondSetArray[j])
    {
      mutualElement += 2;
      break;
	}
  }
}

return sumElement - mutualElement;
}
module.exports.distance = distance